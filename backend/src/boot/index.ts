import { runMongoTasks } from './runMongoTasks';
import startMongo from './startMongo';
import { startServer } from './startServer';
import { startSocket } from './startSocket';
import Youtube from "youtube.ts"

export const runBootTasks = async () => {
    try {
        await startMongo();
        await startServer();
        startSocket();
        await runMongoTasks()
        console.log('Boot succeed!');
    } catch (error) {
        console.log('run boot tasks error', error)
    }
}