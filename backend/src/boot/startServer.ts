import { app } from "../config/expressServer";
import { vars } from "../config/vars";

const { PORT } = vars;

export const startServer = async (): Promise<void> => {
    try {
        app.listen(PORT, () => console.log(`Server is listening on ${PORT}`));
    } catch (error) {
        console.log('Start server error', error);
    }
}