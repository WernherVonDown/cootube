import { io } from "../config/socket";
import { vars } from "../config/vars";

export const startSocket = () => {
    console.log('socket:running:start');
    io.listen(vars.socketPort);
    console.log(`socket:running:complete; started on port: ${vars.socketPort}`);
};