export interface IDevicePermissions {
    audio: boolean;
    video: boolean;
}
