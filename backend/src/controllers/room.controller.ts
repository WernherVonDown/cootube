import { NextFunction, Request, Response } from "express";
import { roomService } from "../services/room.service";

class RoomController {
    async create(req: Request, res: Response, next: NextFunction) {
        try {
            const roomData = await roomService.create();
            return res.json(roomData);
        } catch (error) {
            console.log('RoomController.create error;', error);
            next(error);
        }
    }

    async getRoom(req: Request, res: Response, next: NextFunction) {
        try {
            const roomId = req.params.roomId;
            const roomData = await roomService.getRoom(roomId);
            return res.json(roomData);
        } catch (error) {
            console.log('RoomController.getRoom error;', error);
            next(error);
        }
    }
}

export const roomController = new RoomController();
