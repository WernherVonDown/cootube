import { Schema, model } from 'mongoose';

const Room = new Schema({
    roomId: {
        type: String,
        unique: true,
        required: true
    },
    videoUrl: { type: String, default: '' },
    users: [
        {
            userName: { type: String },
            socketId: { type: String },
            devices: {
                audio: {type: Boolean, default: true},
                video: {type: Boolean, default: true},
            }
        }
    ]
});

Room.methods.toDto = function toDto () {
    return {
        roomId: this.roomId,
        users: this.users,
        videoUrl: this.videoUrl,
    }
}

export default model('Room', Room);