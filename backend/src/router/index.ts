import { Router } from "express";
import userRoutes from './user/router';
import roomRoutes from './room/router';

const router = Router();

router.use('/user', userRoutes);
router.use('/rooms', roomRoutes);

export default router;