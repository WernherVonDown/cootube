import { Router } from "express";
import { roomController } from "../../controllers/room.controller";

const router = Router();

router.post('/create', roomController.create);
router.get('/:roomId', roomController.getRoom);

export default router;