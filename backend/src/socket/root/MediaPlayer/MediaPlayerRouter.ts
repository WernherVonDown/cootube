
import { Router } from "../../../utils/socket/router";
import * as controller from "./mediaPlayerController";


const router = new Router();

router.addRoute({ path: "videoUrl" }, controller.videoUrl);
router.addRoute({ path: "getCurrentVideoUrl" }, controller.getCurrentVideoUrl);
router.addRoute({ path: "mediaPlayerAction" }, controller.mediaPlayerAction);

export const mediaPlayerRouter = router;