import { MediaPlayerEvents } from "../../../const/mediaPlayer/MediaPlayerEvents";
import { roomService } from "../../../services/room.service";
import { IRouteFn } from "../../../types/socket";
import { getSessionRoomId } from "../../../utils/helpers/getSessionRoomId";
import { emitter } from "../../../utils/socket/emitter";
import { ISocketQuery } from "../room/controller";

export const videoUrl: IRouteFn = async (socket, data) => {
    const { roomId }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const { videoUrl } = data;
    await roomService.setVideoUrl({ roomId, videoUrl });
    const sessionRoomId = getSessionRoomId(roomId);
    emitter.sendToRoomButUser(socket, sessionRoomId, MediaPlayerEvents.videoUrl, { videoUrl });
    return {};
}

export const getCurrentVideoUrl: IRouteFn = async (socket, data) => {
    const { roomId }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const videoUrl = await roomService.getVideoUrl({ roomId });
    return { videoUrl};
}

export const mediaPlayerAction: IRouteFn = async (socket, data) => {
    const { roomId }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const sessionRoomId = getSessionRoomId(roomId);
    emitter.sendToRoomButUser(socket, sessionRoomId, MediaPlayerEvents.mediaPlayerAction, data);
    return {};
}