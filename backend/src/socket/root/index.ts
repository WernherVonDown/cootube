import { Router } from "../../utils/socket/router";
import { mediaPlayerRouter } from "./MediaPlayer/MediaPlayerRouter";
import { roomRouter } from "./room/router";
import { textChatRouter } from "./textChat/textChatRouter";
import { videoChatRouter } from "./videoChat/videoChatRouter";


const router = new Router();

router.addRouter("room", roomRouter);

router.addRouter("videoChat", videoChatRouter);
router.addRouter("textChat", textChatRouter);
router.addRouter("mediaPlayer", mediaPlayerRouter);

export const rootRouter = router;