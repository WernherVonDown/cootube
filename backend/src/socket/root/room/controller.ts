import { RoomEvents } from "../../../const/room/ROOM_EVENTS";
import { roomService } from "../../../services/room.service";
import { IRouteFn } from "../../../types/socket";
import { getSessionRoomId } from "../../../utils/helpers/getSessionRoomId";
import { getUserRoomId } from "../../../utils/helpers/getUserRoomId";
import { emitter } from "../../../utils/socket/emitter";

export interface ISocketQuery {
    roomId: string;
    userName: string;
}

export const join: IRouteFn = async (socket, data) => {
    const { roomId, userName }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const { devices } = data;
    const socketId = socket.id;
    const sessionRoomId = getSessionRoomId(roomId);
    const userRoomId = getUserRoomId(socketId);

    socket.join(sessionRoomId);
    socket.join(userRoomId);
    const { users } = await roomService.join({ userName, roomId, socketId, devices });
    emitter.sendToRoomButUser(socket, sessionRoomId, RoomEvents.join, { userName, socketId, devices })
    return { users, user: { userName, socketId } };
}

export const leave: IRouteFn = async (socket, data) => {
    const { roomId, userName }: ISocketQuery = socket.handshake.query as unknown as ISocketQuery;
    const socketId = socket.id;
    const sessionRoomId = getSessionRoomId(roomId);
    const userRoomId = getUserRoomId(socketId);

    socket.leave(sessionRoomId);
    socket.leave(userRoomId);

    await roomService.leave({ roomId, socketId })
    emitter.sendToRoomButUser(socket, sessionRoomId, RoomEvents.leave, { userName, socketId })
    return {}
}