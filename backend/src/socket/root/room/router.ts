import { Router } from "../../../utils/socket/router";
import * as controller from "./controller";


const router = new Router();

router.addRoute({ path: "join" }, controller.join);
router.addRoute({ path: "leave" }, controller.leave);

export const roomRouter = router;