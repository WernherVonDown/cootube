
import { Router } from "../../../utils/socket/router";
import * as controller from "./textChatController";


const router = new Router();

router.addRoute({ path: "textChatMessage" }, controller.onTextMessage);

export const textChatRouter = router;