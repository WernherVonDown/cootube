export const getSessionRoomId = (sessionId: string): string =>
    `room_${sessionId}`;
