export const getUserRoomId = (sessionId: string): string =>
    `room_${sessionId}`;
