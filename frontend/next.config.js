/** @type {import('next').NextConfig} */

require('dotenv').config();

const publicRuntimeConfig = {
    socketUrl: process.env.NEXT_PUBLIC_SOCKET_URL || 'http://localhost:3008/',
    socketPath: process.env.NEXT_PUBLIC_SOCKET_PATH || '/socket.io/',
    apiUrl: process.env.NEXT_PUBLIC_API_URL || "http://localhost:8000/api",
}

const nextConfig = {
  publicRuntimeConfig,
  reactStrictMode: false,
}

module.exports = nextConfig
