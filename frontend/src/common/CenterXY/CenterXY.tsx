import React, { ReactNode } from 'react';
import styles from './CenterXY.module.scss';

interface IProps {
    children: ReactNode | ReactNode[]
}

export const CenterXY: React.FC<IProps> = ({ children }) => {
    return (
        <div className={styles.center}>
            {children}
        </div>
    )
}