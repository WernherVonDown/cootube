import React, { useLayoutEffect, useRef } from 'react'
import { emptyFunc } from '../../helpers/emptyFunc';

interface IProps {
    stream: MediaStream;
    muted?: boolean;
    width?: string | number;
    height?: string | number;
    className?: string;
    onCanPlay?: () => void;
    onEnded?: () => void;
}

export const Video: React.FC<IProps> = (props) => {
    const {
        stream,
        muted,
        width = '100%',
        height = '100%',
        className = '',
        onCanPlay = emptyFunc,
        onEnded = emptyFunc,
        ...restProps
    } = props;
    const videoRef = useRef<any>(null);

    useLayoutEffect(() => {
        if (videoRef.current) {
            videoRef.current.srcObject = stream;
        }
    }, [stream]);

    return (
        <video
            className={className}
            onCanPlay={onCanPlay}
            onEnded={onEnded}
            ref={videoRef}
            poster={'/assets/video-loader.gif'}
            {...restProps}
            muted={muted}
            width={width}
            height={height}
            autoPlay
        />
    )
}
