import React, { useCallback, useContext, useEffect, useRef, useState } from "react";
import ReactPlayer from 'react-player';
import { MediaPlayerContext } from "../../context/MediaPlayerContext";
import getYouTubeID from 'get-youtube-id';
import { Youtube } from "../../utils/youtube/Youtube";
import { MediaPlayerActionTypes } from "../../const/mediaPlayer/MediaPlayerActionTypes";
import { useMediaSocketSubscribe } from "../../hooks/useMediaSocketSubscribe";
import { MediaPlayerEvents } from "../../const/mediaPlayer/MediaPlayerEvents";
import { MediaSocketContext } from "../../context/MediaSocketContext";

export const MediaPlayer: React.FC = () => {
    const { state: { videoUrl, urlProvider } } = useContext(MediaPlayerContext);
    const { actions: { emit } } = useContext(MediaSocketContext);
    const mediaPlayerRef = useRef<ReactPlayer>(null);
    const [volume, setVolume] = useState(1);
    const [playbackRate, setPlaybackRate] = useState(1);
    const [ready, setReady] = useState(false);
    const [playing, setPlaying] = useState(false);

    useEffect(() => {
        setReady(false);
    }, [videoUrl])

    useEffect(() => {
        if (videoUrl) {
            Youtube.getVideoInfo(videoUrl)
        }
    }, [videoUrl])

    const getCurrentTime = useCallback(() => {
        if (mediaPlayerRef.current) {
            return mediaPlayerRef.current?.getCurrentTime() || 0;
        }
        return 0;
    }, []);

    const onReady = useCallback((e: any) => {
        setReady(true);
    }, []);

    const onPlay = useCallback(() => {
        if (mediaPlayerRef.current && ready && urlProvider) {
            const time = getCurrentTime()
            emit(MediaPlayerEvents.mediaPlayerAction, { type: MediaPlayerActionTypes.play, data: { time } })
        }

    }, [ready, mediaPlayerRef])

    const onPause = useCallback(() => {
        if (mediaPlayerRef.current && ready && urlProvider) {
            const time = getCurrentTime()
            emit(MediaPlayerEvents.mediaPlayerAction, { type: MediaPlayerActionTypes.pause, data: { time } })
        }
    }, [ready, urlProvider])

    const onSeek = useCallback(() => {
        console.log('onSeek')
    }, [ready, urlProvider])

    const onProgress = useCallback((e: any) => {
        // console.log('onProgress', e)
    }, []);

    const onError = useCallback((e: any) => {
        console.log('onError', e)
    }, [])

    const onPlaybackRateChange = useCallback((e: number) => {
        if (mediaPlayerRef.current && ready && urlProvider) {
            emit(MediaPlayerEvents.mediaPlayerAction, { type: MediaPlayerActionTypes.playbackRate, data: e })
        }
    }, [ready, urlProvider]);

    const play = useCallback(() => {
        const internalPlayer = mediaPlayerRef.current?.getInternalPlayer();
        if (internalPlayer && ready) {
            const play = internalPlayer.play || internalPlayer.playVideo;
            play()
        }
    }, [mediaPlayerRef, ready]);

    const pause = useCallback(() => {
        const internalPlayer = mediaPlayerRef.current?.getInternalPlayer();
        if (internalPlayer && ready) {
            try {
                const pause = internalPlayer.pauseVideo || internalPlayer.pause;
            pause()
            } catch (error) {
                console.log("ERROR PAUSE", error, pause)
            }
            
        }
    }, [mediaPlayerRef, ready]);

    const seekTo = useCallback((data: number) => {
        const internalPlayer = mediaPlayerRef.current?.getInternalPlayer();
        if (internalPlayer) {
            internalPlayer.seekTo(data);
        }
    }, [mediaPlayerRef]);

    const onMediaPlayerAction = useCallback(({ type, data }: { type: string, data: any }) => {
        if (ready && mediaPlayerRef.current && !urlProvider) {
            if (type === MediaPlayerActionTypes.play) {
                seekTo(data.time);
                // play()
                setPlaying(true)
            }

            if (type === MediaPlayerActionTypes.pause) {
                // seekTo(data.time);
                // pause()
                setPlaying(false)
            }

            if (type === MediaPlayerActionTypes.playbackRate) {
                setPlaybackRate(data)
            }

            if (type === MediaPlayerActionTypes.volume) {
                setVolume(data)
            }
        }
    }, [mediaPlayerRef, ready, urlProvider]);

    useMediaSocketSubscribe(MediaPlayerEvents.mediaPlayerAction, onMediaPlayerAction);

    return (
        <ReactPlayer
            ref={mediaPlayerRef}
            style={{ pointerEvents: !urlProvider ? 'none' : undefined }}
            width={'100%'}
            controls={urlProvider}
            height={'100%'}
            url={videoUrl}
            onReady={onReady}
            onPlay={onPlay}
            onPause={onPause}
            onSeek={onSeek}
            volume={urlProvider ? undefined :volume}
            playing={urlProvider ? undefined : playing}
            onProgress={onProgress}
            playbackRate={urlProvider ? undefined : playbackRate}
            onError={onError}
            onPlaybackRateChange={onPlaybackRateChange}
        />
    )
}