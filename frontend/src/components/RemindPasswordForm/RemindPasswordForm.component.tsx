import { Button, Grid, Paper, TextField, Typography } from "@mui/material"
import { useInput } from '../../hooks/useInput';
import { useCallback, useContext, useState } from 'react';
import { AuthContext } from "../../context/AuthContext";
import styles from "./RemindPasswordForm.module.scss"
import Link from "next/link";
import { RemindPasswordRoute } from "../../const/API_ROUTES";

export const RemindPasswordForm = () => {
    const { actions: { resetPassword } } = useContext(AuthContext);
    const [resetError, setResetError] = useState(false);
    const email = useInput('');

    const onResetPassword = useCallback(async () => {
        if (!email.value.length) {
            return alert('Заполните почту');
        }
        interface IRes {
            success: boolean
        }
        const res = await resetPassword(email.value) as IRes;
        if (!res.success) {
            setResetError(true)
        }
    }, [email.value]);

    return (
        <Paper>
            <div className={styles.remindPasswordForm}>
                {resetError ?
                    <Grid>
                        <Typography variant="h5">Ошибка восстановления проверьте введенные данные</Typography>
                        <Button fullWidth sx={{ marginTop: 1 }} variant='contained' href={RemindPasswordRoute}>
                            Попробовать еще раз
                        </Button>
                    </Grid>
                    : <Grid>
                        <Typography variant='h5'>Восстановление пароля</Typography>
                        <TextField variant="outlined" margin='normal' fullWidth size='small' {...email} type={'text'} label='Ваша почта' />
                        <Button fullWidth sx={{ marginTop: 1 }} variant='contained' onClick={onResetPassword}>
                            вход
                        </Button>
                    </Grid>

                }
            </div>
        </Paper>
    )

}

