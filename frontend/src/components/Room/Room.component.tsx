import React, { useContext } from "react";
import { RoomContext } from "../../context/RoomContext";
// import { TextChatWidget } from "../TextChatWidget/TextChatWidget.component";
import styles from "./Room.module.scss";

import dynamic from 'next/dynamic'
import { Suspense } from 'react'
import { VideoChatLayout } from "../VideoChatLayout/VideoChatLayout.component";
import { MediaPlayer } from "../MediaPlayer/MediaPlayer.component";
import { VideoChatPositions } from "../../const/videoChat/VideoChatPositions";
import { MediaPlayerContext } from "../../context/MediaPlayerContext";
//@ts-ignore https://nextjs.org/docs/advanced-features/dynamic-import
const TextChatWidget = dynamic(() => import('../TextChatWidget/TextChatWidget.component').then((mod) => mod.TextChatWidget), { ssr: false });

interface IProps {

}

export const Room: React.FC<IProps> = () => {
    const { state: { userName } } = useContext(RoomContext);
    const { state: { videoUrl } } = useContext(MediaPlayerContext);
    return (
        <div className={styles.roomWrapper}>
            {videoUrl ? <VideoChatLayout videoPosition={VideoChatPositions.right}>
                <MediaPlayer />
            </VideoChatLayout>
                : <VideoChatLayout />
            }
            <Suspense fallback={`Loading...`}>
                <TextChatWidget />
            </Suspense>
        </div>
    )
}
