import { AppBar, Box, Button, Container, Link, Toolbar, Typography, styled, Grid, IconButton, TextField, InputBase, SvgIcon } from "@mui/material";
import React, { ReactNode, useContext, useMemo } from "react";
import { RoomContext } from "../../context/RoomContext";
import styles from "./RoomLayout.module.scss"
import PlayCircleFilledWhiteIcon from '@mui/icons-material/PlayCircleFilledWhite';
import { VideoUrlForm } from "../VideoUrlForm/VideoUrlForm.component";

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

interface IProps {
    children: ReactNode | ReactNode[];
}


export const RoomLayout: React.FC<IProps> = ({ children }) => {
    const { state: { users } } = useContext(RoomContext);
    const numberOfUsers = useMemo(() => {
        return users.length;
    }, [users.length]);

    return (
        <div className={styles.roomLayout}>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography component="div" variant="h4">
                            Cootube
                        </Typography>
                        <Typography marginLeft={1} component="div" fontSize={{ xs: '16px', md: '20px', lg: '20x' }} noWrap>
                            Пользователи: {numberOfUsers}
                        </Typography>
                        <VideoUrlForm hideForMobile={true} />
                        <Box sx={{ flexGrow: 1 }}></Box>
                        <Button color="inherit" href="/">Выйти</Button>
                    </Toolbar>
                </AppBar>
            </Box>

            {/* <Container maxWidth={false}> */}
            {/* <Offset /> */}
            <div className={styles.roomContent}>
                {children}
            </div>
            <Box sx={{ flexGrow: 1 }} display={{ sm: 'none' }}>
                <AppBar position="static">
                    <Toolbar>
                        {/* <Typography component="div" fontSize={{xs: '12px', md: '20px',  lg: '20x'}} noWrap>
                            Пользователи: {numberOfUsers}
                        </Typography> */}
                        <VideoUrlForm />
                        <Box sx={{ flexGrow: 1 }}></Box>
                        {/* <Button color="inherit" href="/">Выйти</Button> */}
                    </Toolbar>
                </AppBar>
            </Box>
            {/* </Container> */}
        </div>
    )
}
