
import React, { ReactNode } from "react";
import { VideoChatPositions } from "../../const/videoChat/VideoChatPositions";
import { VideoChat } from "../VideoChat/VideoChat.component";
import { makeStyles, createStyles } from "@mui/styles";
import { Grid, Theme } from "@mui/material";

interface IProps {
    videoPosition?: VideoChatPositions;
    children?: ReactNode | ReactNode[];
}

// const useStyles: any = makeStyles((theme: Theme) => createStyles({
//     videos: {
//         // width: "20%",
//         [theme.breakpoints.down('md')]: {
//             // width: "lg",
//             backgroundColor: 'green',
//         },
//         [theme.breakpoints.up('md')]: {
//             backgroundColor: 'green',
//             // width: "100%",
//         },
//         [theme.breakpoints.down('md')]: {
//             // flexDirection: 'column',
//             backgroundColor: 'black',
//         },
//         [theme.breakpoints.up('lg')]: {
//             // width: "100%",
//             backgroundColor: 'green',
//         },
//     },
//     container: {
//         [theme.breakpoints.down('xs')]: {
//             // flexDirection: 'column',
//             backgroundColor: 'blue',
//         },
//         [theme.breakpoints.down('md')]: {
//             // flexDirection: 'column',
//             backgroundColor: 'black',
//         },
//         [theme.breakpoints.up('lg')]: {
//             backgroundColor: 'blue',
//         },
//     },
//     content: {
//         // width: "80%",
//         [theme.breakpoints.down('md')]: {
//             backgroundColor: 'red',
//         },
//         [theme.breakpoints.up('md')]: {
//             backgroundColor: 'red',
//         },
//         [theme.breakpoints.up('lg')]: {
//             backgroundColor: 'red',
//         },
//     },
// }));

export const VideoChatLayout: React.FC<IProps> = ({ videoPosition = VideoChatPositions.fullscreen, children }) => {
    // const classes = useStyles({});

    if (videoPosition === VideoChatPositions.right) {
        return (
            <Grid container display="flex" direction={{md: 'row', lg: 'row', xs:"column"}} >
                <Grid item
                // md={9} xs={12}
                xs={9}
                md={8.5}
                sm={9.5}
                lg={9.8}

                >
                    {children}
                </Grid>
                <Grid item 
                // xs={12} md={3} 
                xs={3}
                md={3.5}
                sm={2.5}
                lg={2.2}
               >
                    <VideoChat />
                </Grid>
            </Grid>
        )
    }
    return <VideoChat />
}
