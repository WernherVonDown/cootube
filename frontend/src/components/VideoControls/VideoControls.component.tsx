import { IconButton, SvgIcon } from "@mui/material";
import React, { useContext } from "react";
import styles from "./VideoControls.module.scss";
import { StreamContext } from "../../context/StreamContext";
import BlurOnIcon from '@mui/icons-material/BlurOn';
import BlurOffIcon from '@mui/icons-material/BlurOff';
import VideocamIcon from '@mui/icons-material/Videocam';
import KeyboardVoiceIcon from '@mui/icons-material/KeyboardVoice';
import MicOffIcon from '@mui/icons-material/MicOff';
import VideocamOffIcon from '@mui/icons-material/VideocamOff';
import classNames from "classnames";

export const VideoControls: React.FC = () => {
    const { actions: { setDevicePermissions, setBlurVideo }, state: { blurVideo, devicePermissions } } = useContext(StreamContext);
    return (
        <div className={styles.videoControls}>
            <IconButton onClick={() => setBlurVideo((p: boolean) => !p)} className={classNames(styles.videoControlsItem, styles.videoControlsBlurBtn)} size={'medium'} aria-label="Example" >
                <SvgIcon fontSize="large">{blurVideo ? <BlurOnIcon /> : <BlurOffIcon />}</SvgIcon>
            </IconButton>
            <IconButton onClick={() => { setDevicePermissions((p: any) => ({ ...p, video: !p.video })) }} className={styles.videoControlsItem} size={'medium'} aria-label="Example" >
                <SvgIcon fontSize="large">{devicePermissions.video ? <VideocamIcon /> : <VideocamOffIcon />}</SvgIcon>
            </IconButton>
            <IconButton onClick={() => { setDevicePermissions((p: any) => ({ ...p, audio: !p.audio })) }} className={styles.videoControlsItem} size={'medium'} aria-label="Example" >
                <SvgIcon fontSize="large">{devicePermissions.audio ? <KeyboardVoiceIcon /> : <MicOffIcon />}</SvgIcon>
            </IconButton>
        </div>
    )
}
