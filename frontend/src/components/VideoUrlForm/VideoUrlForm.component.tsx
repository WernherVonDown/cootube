import { Box, styled, IconButton, InputBase, SvgIcon } from "@mui/material";
import React, { useCallback, useContext, useEffect } from "react";
import styles from "./VideoUrlForm.module.scss"
import PlayCircleFilledWhiteIcon from '@mui/icons-material/PlayCircleFilledWhite';
import { useInput } from "../../hooks/useInput";
import { MediaPlayerContext } from "../../context/MediaPlayerContext";
import CancelSharpIcon from '@mui/icons-material/CancelSharp';
import CloseSharpIcon from '@mui/icons-material/CloseSharp';
import classNames from "classnames";

const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: 'inherit',
    '& .MuiInputBase-input': {
        padding: theme.spacing(1, 1, 1, 0),
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(0em + ${theme.spacing(1)})`,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: '20ch',
        },
        background: "#125fab8f",
        borderRadius: '6px',
        marginLeft: '0.5rem',

    }
}));

interface IProps {
    hideForMobile?: boolean;
}

export const VideoUrlForm: React.FC<IProps> = ({ hideForMobile }) => {
    const { actions: { setVideoUrl, setUrlProvider, clearVideoUrl }, state: { videoUrl } } = useContext(MediaPlayerContext);
    const videoUrlInput = useInput('');

    useEffect(() => {
        if (videoUrl && videoUrlInput.value !== videoUrl) {
            videoUrlInput.onChange({ target: { value: videoUrl } });
        }
    }, [videoUrl, videoUrlInput.value])

    const handleVideoUrl = useCallback(() => {
        if (videoUrlInput.value.trim().length) {
            setVideoUrl(videoUrlInput.value);
            setUrlProvider(true);
        }
    }, [videoUrlInput.value]);


    return (
        <Box alignItems={"center"} className={styles.container} display={{ xs: hideForMobile ? 'none' : 'flex', sm: 'flex' }}>
            <StyledInputBase
                className={'topHeader'}
                placeholder="Youtube link..."
                inputProps={{ 'aria-label': 'search' }}
                {...videoUrlInput}
                disabled={!!videoUrl}
            />
            {!videoUrl && <IconButton onClick={handleVideoUrl} className={classNames(styles.playButton, styles.btn)} size={'medium'} aria-label="Example" >
                <SvgIcon fontSize="large"><PlayCircleFilledWhiteIcon /></SvgIcon>
            </IconButton>}
            {videoUrl && <IconButton onClick={clearVideoUrl} className={classNames(styles.closeButton, styles.btn)} size={'medium'} aria-label="Example" >
                <SvgIcon fontSize="medium"><CancelSharpIcon /></SvgIcon>
            </IconButton>}
        </Box>
    )
}