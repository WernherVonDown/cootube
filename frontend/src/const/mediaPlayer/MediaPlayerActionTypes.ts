export enum MediaPlayerActionTypes {
    play = 'play',
    pause = 'pause',
    playbackRate = 'playbackRate',
    playbackPosition = 'playbackPosition',
    volume = 'volume',
}