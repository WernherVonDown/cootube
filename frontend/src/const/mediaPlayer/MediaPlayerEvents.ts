export enum MediaPlayerEvents {
    videoUrl = 'mediaPlayer:videoUrl',
    getCurrentVideoUrl = 'mediaPlayer:getCurrentVideoUrl',
    mediaPlayerAction = 'mediaPlayer:mediaPlayerAction',
}
