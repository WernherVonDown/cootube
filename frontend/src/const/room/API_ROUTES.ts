const BASE = 'rooms';

export const RoomApiRoutes = {
    CREATE: `${BASE}/create`,
    getRoomAPI: (roomId: string) => `${BASE}/${roomId}`,
}