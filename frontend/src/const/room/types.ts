export interface IRoomCreateResponse {
    roomId: string;
}

export interface IRoomResponse {
    roomId: string;
}