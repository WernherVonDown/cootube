export interface IRemoteStream {
    userName: string;
    userId: string;
    stream: MediaStream;
}

export interface IDevicePermissions {
    audio: boolean;
    video: boolean;
}
