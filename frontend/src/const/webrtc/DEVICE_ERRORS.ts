export enum DeviceErrors {
    NotAllowedError = 'NotAllowedError',
    PermissionDeniedError = 'PermissionDeniedError',
    NotReadableError = 'NotReadableError',
    TrackStartError = 'TrackStartError',
    NotFoundError = 'NotFoundError',
    DevicesNotFoundError = 'DevicesNotFoundError',
    Unknown = 'Unknown',
}

export const DeviceErrorsDenied = [
    DeviceErrors.NotAllowedError,
    DeviceErrors.PermissionDeniedError,
];
export const DeviceErrorsBusy = [
    DeviceErrors.NotReadableError,
    DeviceErrors.TrackStartError,
];
export const DeviceErrorsNotConnected = [
    DeviceErrors.NotFoundError,
    DeviceErrors.DevicesNotFoundError,
];
