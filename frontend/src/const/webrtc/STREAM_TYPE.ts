export enum STREAM_TYPE {
  VIDEO_CHAT = 'video.chat',
  SCREEN_SHARING = 'screen.sharing',
};
