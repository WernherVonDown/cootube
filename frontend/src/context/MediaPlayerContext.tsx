import React, { ReactElement, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { MediaPlayerEvents } from '../const/mediaPlayer/MediaPlayerEvents';
import { useMediaSocketSubscribe } from '../hooks/useMediaSocketSubscribe';
import { MediaSocketContext } from './MediaSocketContext';

interface IState {
    videoUrl: string;
    urlProvider: boolean;
}

interface IProps {
    state: IState,
    children: ReactElement | ReactElement[]
}

interface ContextValue {
    state: IState,
    actions: {
        [key: string]: (...args: any[]) => unknown
    }
}

const MediaPlayerContext = React.createContext({} as ContextValue);

const MediaPlayerContextProvider = (props: IProps) => {
    const { children } = props;
    const { actions: { emit }, state: { isConnected } } = useContext(MediaSocketContext);
    const [videoUrl, setVideoUrl] = useState('');
    const [urlProvider, setUrlProvider] = useState(false);

    useEffect(() => {
        console.log('LOL', videoUrl, urlProvider)
        if (urlProvider) {
            emit(MediaPlayerEvents.videoUrl, { videoUrl })
        }
    }, [videoUrl]);

    useEffect(() => {
        if (isConnected) {
            getCurrentVideoUrl();
        }
    }, [isConnected]);

    const clearVideoUrl = useCallback(() => {
        setVideoUrl('')
        emit(MediaPlayerEvents.videoUrl, { videoUrl: '' })
    }, [])

    const getCurrentVideoUrl = useCallback(async () => {
        console.log('getCurrentVideoUrl')
        const res = await emit(MediaPlayerEvents.getCurrentVideoUrl);
        console.log('CURRENT', res)
        setVideoUrl(res?.videoUrl);
    }, [])

    const onVideoUrl = useCallback(({ videoUrl }: { videoUrl: string }) => {
        console.log("ON VIDEO URL", videoUrl)
        setVideoUrl(videoUrl)
        setUrlProvider(false);
    }, []);

    useMediaSocketSubscribe(MediaPlayerEvents.videoUrl, onVideoUrl);

    const state = useMemo(() => ({
        videoUrl,
        urlProvider,
    }), [
        videoUrl,
        urlProvider,
    ]);

    const actions = {
        setVideoUrl,
        setUrlProvider,
        clearVideoUrl,
    }


    return <MediaPlayerContext.Provider
        value={{
            state,
            actions
        }}
    >
        {children}
    </MediaPlayerContext.Provider>
}

MediaPlayerContextProvider.defaultProps = {
    state: {
    }
}


export { MediaPlayerContext, MediaPlayerContextProvider };
