import { useContext, useEffect } from 'react';
import { MediaSocketContext } from '../context/MediaSocketContext';

export const useMediaSocketSubscribe = (event: string, handler: (...args: any[]) => void) => {
    const {
        state: { isConnected },
        actions: { unsubscribe, subscribe },
    } = useContext(MediaSocketContext);

    useEffect(() => {
        if (isConnected) {
            subscribe(event, handler);
        }

        return () => {
            unsubscribe(event, handler);
        };
    }, [isConnected, event, handler]);
};
