import { AppBar, Container, styled, Toolbar } from "@mui/material";
import React from "react";
import { CenterXY } from "../common/CenterXY/CenterXY";
import { LoginForm } from '../components/LoginForm/LoginForm.component';
import { MainLayout } from "../components/MainLayout/MainLayout";

const Offset = styled('div')(({ theme }) => theme.mixins.toolbar);

export default function Login() {
    return (
        <MainLayout>
            <CenterXY>
                <LoginForm />
            </CenterXY>
        </MainLayout>
    )
}
