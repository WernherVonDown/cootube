import { AxiosResponse } from "axios";
import { RoomApiRoutes } from "../const/room/API_ROUTES";
import { IRoomCreateResponse, IRoomResponse } from "../const/room/types";
import $api from "../http";

export default class RoomService {
    static async create(): Promise<AxiosResponse<IRoomCreateResponse>> {
        return $api.post<IRoomCreateResponse>(RoomApiRoutes.CREATE);
    }

    static async get(roomId: string): Promise<AxiosResponse<IRoomResponse>> {
        return $api.get<IRoomResponse>(RoomApiRoutes.getRoomAPI(roomId));
    }
}