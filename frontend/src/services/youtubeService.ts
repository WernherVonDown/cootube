import axios, { AxiosResponse } from "axios";
import { RoomApiRoutes } from "../const/room/API_ROUTES";
import { IRoomCreateResponse, IRoomResponse } from "../const/room/types";

const GOOGLE_API_KEY = 'AIzaSyB9-OQmS69rnClhTanr7Ac0t73jOkmHEEY';
const GOOGLE_API_URL = 'https://www.googleapis.com/youtube/v3'
const $youtubebeApi = axios.create({
    baseURL: GOOGLE_API_URL
});
// `/videos?id=7lCDEYXw3mM&key=AIzaSyB9-OQmS69rnClhTanr7Ac0t73jOkmHEEY&part=snippet`
export default class YoutubeService {
    static async getVideoInfo(videoId: string): Promise<AxiosResponse<any>> {
        return $youtubebeApi.get<any>(`/videos?id=${videoId}&key=${GOOGLE_API_KEY}&part=snippet`);
    }

    // static async get(roomId: string): Promise<AxiosResponse<IRoomResponse>> {
    //     return $api.get<IRoomResponse>(RoomApiRoutes.getRoomAPI(roomId));
    // }
}