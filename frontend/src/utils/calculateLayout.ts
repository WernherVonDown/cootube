export const calculateLayout = (
    containerWidth: number,
    containerHeight: number,
    reactsCount: number,
    aspectRatio: number
): { width: number; height: number; cols: number, rows: number } => {
    let bestLayout = {
        area: 0,
        cols: 0,
        rows: 0,
        width: 0,
        height: 0
    };

    for (let cols = 1; cols <= reactsCount; cols++) {
        const rows = Math.ceil(reactsCount / cols);
        const hScale = containerWidth / (cols * aspectRatio);
        const vScale = containerHeight / rows;
        let width;
        let height;
        if (hScale <= vScale) {
            width = Math.floor(containerWidth / cols);
            height = Math.floor(width / aspectRatio);
        } else {
            height = Math.floor(containerHeight / rows);
            width = Math.floor(height * aspectRatio);
        }
        const area = width * height;
        if (area > bestLayout.area) {
            bestLayout = {
                area,
                width,
                height,
                rows,
                cols
            };
        }
    }
    return bestLayout;
}
