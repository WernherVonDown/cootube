

export const getBluredVideo = async (videoStream: MediaStream) =>{
    const { EffectBlur, VideoEffects } = require('@vkontakte/calls-video-effects');
    try {
        const effectBlur = new EffectBlur(), videoEffects = new VideoEffects();
        const track = await videoEffects.setEffect(effectBlur, videoStream.getVideoTracks()[0]);
        let newStream;
        if (videoStream.getAudioTracks().length) {
            newStream = new MediaStream([track, videoStream.getAudioTracks()[0]]);
        } else {
            newStream = new MediaStream([track]);
        }
        //@ts-ignore
        newStream.blured = true;
        return newStream
    } catch (error: any) {
        console.log(`getBluredVideo error: [${error?.name}] ${error?.message}`);
        return videoStream;
    }
}
