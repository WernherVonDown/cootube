import getYouTubeID from 'get-youtube-id';
import YoutubeService from '../../services/youtubeService';

export class Youtube {
    static async getVideoInfo(url: string) {
        console.log("YOUTUBE ID", getYouTubeID(url))
        try {
            const videoId = getYouTubeID(url);
            if (!videoId) {
                throw "Incorrect link"
            }
            const res = await YoutubeService.getVideoInfo(videoId);
            const snippet = res.data.items[0]?.snippet;
            console.log("RESSS", snippet)
            const { title } = snippet;
            return { title };
        } catch (error) {
            console.log("Youtube.getVideoInfo error", error);
        }
    }
}